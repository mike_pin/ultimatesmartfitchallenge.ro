<?php
require_once '_init.php';
require_once 'includes/_html_header.php';
require_once 'includes/_header.php';
?>

<main role="main">

	

	<div class="bar">
		<div class="container">
			<h1><?=LOGIN_PTITLE;?></h1>
			<div class="row">
				<div class="col-md-8">
					<form class="needs-validation" novalidate>
						<div class="row">
							<div class="col-lg-8 offset-lg-2">
								<div class="form-group">
									<label for=""><?=USERNAME_LABEL;?><span class="text-danger">*</span></label>
									<input type="text" class="form-control" placeholder="<?=USERNAME_PLACEHOLDER;?>" required>
								</div>
								<div class="form-group">
									<label for=""><?=PASSWORD_LABEL;?><span class="text-danger">*</span></label>
									<input type="password" class="form-control" placeholder="<?=PASSWORD_PLACEHOLDER;?>" required>
								</div>
								<div class="form-group">
									<div class="custom-control custom-checkbox">
										<input class="custom-control-input" type="checkbox" value="" id="keep-signedin">
										<label class="custom-control-label" for="keep-signedin"><?=KEEP_SIGNED_IN;?></label>
									</div>
								</div>
								<div class="mt-4 text-center">
									<button type="submit" class="btn btn-primary"><?=LOGIN;?></button>
									<a href="<?=HOST;?>register.php" class="btn btn-secondary"><?=REGISTER;?></a>
									<br>
									<a href="<?=HOST;?>reset-pass.php" class="d-inline-block mt-2"><?=FORGOT_PASS;?></a>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-4">
					<?php include_once 'z-sidebar.php';?>
				</div>
			</div> <!-- /.row -->
		</div> <!-- /.container -->
	</div> <!-- /.bar -->



</main> <!-- /main -->

<?php
require_once 'includes/_footer.php';
require_once 'includes/_html_footer.php';
require_once '_deinit.php';
?>