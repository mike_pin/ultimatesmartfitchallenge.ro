<li class="plug">
	<div class="plug__cite">
		<div class="row">
			<div class="col-sm-8">
				<blockquote class="blockquote">
					<p>
						Timisoara. Un oras indepartat dar totusi atat de apropiat de suflet! Un oras cu oameni binevoitori, veseli, speciali si atat de scumpi! Un oras unde s-au intamplat multe. S-a intamplat sa gazduiasca prima si cea de-a doaua editie a UltimateSmartfitChallenge intr-una din cele mai de top locatii dedicate sportului, Smartfit Studio.<br>
						Abia astept cea de-a treia editie.
					</p>
					<p>
						YOU ARE THE BEST!<br>
						Adrian Schuller
					</p>
				</blockquote>
			</div>
			<div class="col-sm-4">
				<div class="plug__pic">
					<img src="<?=HOST_CDNA;?>imgs/z-art-register.png" class="img-fluid">
				</div>
				<div class="plug__auth">Adrian Shuller</div>
			</div>
		</div>
	</div>
</li> <!-- /.sb-art -->