<?php
require_once '_init.php';
require_once 'includes/_html_header.php';
require_once 'includes/_header.php';
?>

<main role="main">

	

	<div class="bar">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<h1 class="ev__title">EVENT TITLE</h1>
					<p class="ev__date"><i class="far fa-calendar-alt"></i> <?=POSTED;?> <time datetime="2018-02-02">02 feb 2018</time></p>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
					<div class="row">
						<div class="col-md-4">
							<div class="bsimple">
								<div class="bsimple__ico"><i class="far fa-edit"></i></div>
								<h5 class="bsimple__title">Registrations</h5>
								<div class="bsimple__caption">
									<p>15.01.2018 - 22.03.2018</p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bsimple">
								<div class="bsimple__ico"><i class="far fa-money-bill-alt"></i></div>
								<h5 class="bsimple__title">Registration fee</h5>
								<div class="bsimple__caption">
									<p>
										for qualification: 20 euro<br>
										for semifinals: 40 euro
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bsimple">
								<div class="bsimple__ico"><i class="fas fa-cog"></i></div>
								<h5 class="bsimple__title">Qualifications</h5>
								<div class="bsimple__caption">
									<p>
										WOD 1 - 22.03.2018 - 25.03.2018<br>
										WOD 2 - 29.03.2018 - 01.04.2018<br>
										WOD 3 - 05.04.2018 - 08.04.2018
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bsimple">
								<div class="bsimple__ico"><i class="fas fa-users"></i></div>
								<h5 class="bsimple__title">Categories</h5>
								<div class="bsimple__caption">
									<p>
										Scaled: 30 male / 20 female<br>
										RX: 25 male / 17 female<br>
										+5 greencards males RX / 3 greencards females RX
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bsimple">
								<div class="bsimple__ico"><i class="fas fa-flag-checkered"></i></div>
								<h5 class="bsimple__title">Semifinals and finals</h5>
								<div class="bsimple__caption">
									<p>
										Day 1: 07.07.2018<br>
										Day 2: 08.07.2018
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bsimple">
								<div class="bsimple__ico"><i class="fas fa-map-marker-alt"></i></div>
								<h5 class="bsimple__title">Location</h5>
								<div class="bsimple__caption">
									<p>Victory Square, Timișoara, RO</p>
								</div>
							</div>
						</div>
					</div> <!-- /.row -->
					<div class="bprize">
						<span class="fa-layers fa-fw fa-5x">
							<i class="fas fa-circle"></i>
							<i class="fas fa-trophy" data-fa-transform="shrink-7" style="color:#fff"></i>
						</span><br>
						<b>PRIZES: 5700 €</b>
					</div>
					<div class="row">
						<div class="col-6 col-xl-3">
							<div class="bsimple bsimple--red">
								<div class="bsimple__ico">
									<span class="fa-layers fa-fw">
										<i class="fas fa-circle" style="color:#fff"></i>
										<i class="fas fa-trophy" data-fa-transform="shrink-7" style="color:#f00"></i>
									</span>
								</div>
								<h6 class="bsimple__title">RX - MALE</h6>
								<div class="bsimple__caption">
									<p>1st place: 1000 €</p>
									<p>2nd place: 750 €</p>
									<p>3rd place: 500 €</p>
								</div>
							</div>
						</div>
						<div class="col-6 col-xl-3">
							<div class="bsimple bsimple--red">
								<div class="bsimple__ico">
									<span class="fa-layers fa-fw">
										<i class="fas fa-circle" style="color:#fff"></i>
										<i class="fas fa-trophy" data-fa-transform="shrink-7" style="color:#f00"></i>
									</span>
								</div>
								<h6 class="bsimple__title">RX - FEMALE</h6>
								<div class="bsimple__caption">
									<p>1st place: 1000 €</p>
									<p>2nd place: 750 €</p>
									<p>3rd place: 500 €</p>
								</div>
							</div>
						</div>
						<div class="col-6 col-xl-3">
							<div class="bsimple bsimple--red">
								<div class="bsimple__ico">
									<span class="fa-layers fa-fw">
										<i class="fas fa-circle" style="color:#fff"></i>
										<i class="fas fa-trophy" data-fa-transform="shrink-7" style="color:#f00"></i>
									</span>
								</div>
								<h6 class="bsimple__title">SCALED - MALE</h6>
								<div class="bsimple__caption">
									<p>1st place: 300 €</p>
									<p>2nd place: 200 €</p>
									<p>3rd place: 100 €</p>
								</div>
							</div>
						</div>
						<div class="col-6 col-xl-3">
							<div class="bsimple bsimple--red">
								<div class="bsimple__ico">
									<span class="fa-layers fa-fw">
										<i class="fas fa-circle" style="color:#fff"></i>
										<i class="fas fa-trophy" data-fa-transform="shrink-7" style="color:#f00"></i>
									</span>
								</div>
								<h6 class="bsimple__title">SCALED - FEMALE</h6>
								<div class="bsimple__caption">
									<p>1st place: 300 €</p>
									<p>2nd place: 200 €</p>
									<p>3rd place: 100 €</p>
								</div>
							</div>
						</div>
					</div> <!-- /.row -->
					<div class="text-center">
						<a href="" class="btn btn-lg btn-primary"><i class="far fa-hand-point-right"></i> JOIN THE COMPETITION!</a>
					</div>
					<div class="row no-gutters">
						<div class="col-md-4">
							<div class="bsimple bsimple--red">
								<div class="bsimple__ico bsimple__ico--2x">
									<span class="fa-layers fa-fw">
										<i class="fas fa-circle" style="color:#fff"></i>
										<i class="fas fa-cogs" data-fa-transform="shrink-7" style="color:#f00"></i>
									</span>
								</div>
								<h5 class="bsimple__title">WOD 1</h5>
								<div class="bsimple__caption">
									<p>
										<b>Qualifier WOD 1</b><br>
										has been announced on<br>
										March 22, 2018
									</p>
									<p>
										<b>The Submission deadline</b><br>
										for Qualifier WOD 1 is the<br>
										March 25, 2018
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bsimple bsimple--red">
								<div class="bsimple__ico bsimple__ico--2x">
									<span class="fa-layers fa-fw">
										<i class="fas fa-circle" style="color:#fff"></i>
										<i class="fas fa-cogs" data-fa-transform="shrink-7" style="color:#f00"></i>
									</span>
								</div>
								<h5 class="bsimple__title">WOD 2</h5>
								<div class="bsimple__caption">
									<p>
										<b>Qualifier WOD 2</b><br>
										has been announced on<br>
										March 29, 2018
									</p>
									<p>
										<b>The Submission deadline</b><br>
										for Qualifier WOD 2 is the<br>
										April 1, 2018
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bsimple bsimple--red">
								<div class="bsimple__ico bsimple__ico--2x">
									<span class="fa-layers fa-fw">
										<i class="fas fa-circle" style="color:#fff"></i>
										<i class="fas fa-cogs" data-fa-transform="shrink-7" style="color:#f00"></i>
									</span>
								</div>
								<h5 class="bsimple__title">WOD 3</h5>
								<div class="bsimple__caption">
									<p>
										<b>Qualifier WOD 3</b><br>
										has been announced on<br>
										April 5, 2018
									</p>
									<p>
										<b>The Submission deadline</b><br>
										for Qualifier WOD 1 is the<br>
										April 8, 2018
									</p>
								</div>
							</div>
						</div>
					</div> <!-- /.row -->
					<div class="text-center">
						<a href="" class="btn btn-lg btn-primary"><i class="far fa-edit"></i> REGISTER!</a>
					</div>
				</div>
				<div class="col-md-4">
					<?php include_once 'z-sidebar.php';?>
				</div>
			</div> <!-- /.row -->
		</div> <!-- /.container -->
	</div> <!-- /.bar -->



</main> <!-- /main -->

<?php
require_once 'includes/_footer.php';
require_once 'includes/_html_footer.php';
require_once '_deinit.php';
?>