<?php
require_once '_init.php';
require_once 'includes/_html_header.php';
require_once 'includes/_header.php';
?>

<div id="sf-carousel" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		<li data-target="#sf-carousel" data-slide-to="0" class="active"></li>
		<li data-target="#sf-carousel" data-slide-to="1"></li>
	</ol>
	<div class="carousel-inner text-center">
		<div class="carousel-item active">
			<picture>
				<source media="(min-width: 768px)" srcset="<?=HOST_CDNA;?>imgs/z-slide-1.png">
				<img src="<?=HOST_CDNA;?>imgs/z-slide-1.png" class="img-fluid" alt="">
			</picture>
		</div>
		<div class="carousel-item">
			<picture>
				<source media="(min-width: 768px)" srcset="<?=HOST_CDNA;?>imgs/z-slide-2.png">
				<img src="<?=HOST_CDNA;?>imgs/z-slide-2.png" class="img-fluid" alt="">
			</picture>
		</div>
	</div>
	<a class="carousel-control-prev" href="#sf-carousel" data-slide="prev"><span class="carousel-control-prev-icon"></span></a>
	<a class="carousel-control-next" href="#sf-carousel" data-slide="next"><span class="carousel-control-next-icon"></span></a>
</div> <!-- /#sf-carousel -->

<section class="bar-join bar bar--red text-center text-md-left">
	<div class="container d-flex flex-column flex-md-row justify-content-between align-items-center">
		<h4><?=REGISTRATION_CALL;?></h4>
		<a href="<?=HOST;?>register.php" class="btn btn-lg btn-primary mt-4 mt-md-0 ml-md-2"><?=BTN_REGISTER;?></a>
	</div>
</section> <!-- /.bar-join -->

<main role="main">
	<div class="container">
		<section class="bar mt-5">
			<div class="row">
				<div class="col-lg-6 col-xl-5">
					<h3>ABOUT U.S.C.</h3>
					<p>[U.S.C] Ultimate Smartfit Challenge is a sports competition made by Smartfit. Born in 2014, first as a local contest for Ultimate Training, Functional Training, Crossfit devotees, it was a real success. By the years we developed a concept which is in present grown by more than one section. (more…)</p>
					<p><a href="" class="btn btn-secondary"><?=BTN_MORE;?></a></p>
					<div class="text-center mb-2">
						<h3>ROMANIAN THROWDOWN 2018</h3>
						<div class="countdown2event"></div>
					</div>
				</div>
				<div class="col-lg-6 col-xl-7">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/L2Z7w3fNC_M?rel=0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</section> <!-- /.bar -->
		<hr>
		<section class="bar">
			<div class="row">
				<div class="col-md-4">
					<article class="sf-art">
						<a href="" class="art__pic" title=""><img src="<?=HOST_CDNA;?>imgs/z-art-poster.png" class="img-fluid" alt="ROMANIAN THROWDOWN BY USC – 2018"></a>
						<h2 class="h3 art__title">ROMANIAN THROWDOWN BY USC – 2018</h2>
						<div class="art__caption">
							<p>Romanian Throwdown by USC is a competition growth by the desire to bring together the best athletes from all over the Europe, becoming the biggest sports event that crosses Romanian boards. (more…)</p>
						</div>
						<a href="" class="btn btn-secondary"><?=BTN_MORE;?></a>
					</article>
				</div>
				<div class="col-md-4">
					<article class="sf-art">
						<a href="" class="art__pic" title=""><img src="<?=HOST_CDNA;?>imgs/z-art-qualifiers.png" class="img-fluid" alt="QUALIFIERS"></a>
						<h2 class="h3 art__title">QUALIFIERS</h2>
						<div class="art__caption">
							<p>How to apply? Qualifications are made online, based on videos which contain the execution of the indicated WOD/week.After three weeks of challenges, our judges will publish the results of your performance. Registration opens on 15.01.2018 and closes on 22.03...</p>
						</div>
						<a href="" class="btn btn-secondary"><?=BTN_MORE;?></a>
					</article>
				</div>
				<div class="col-md-4">
					<article class="sf-art">
						<a href="" class="art__pic" title=""><img src="<?=HOST_CDNA;?>imgs/z-art-register.png" class="img-fluid" alt="REGISTER"></a>
						<h2 class="h3 art__title">REGISTER</h2>
						<div class="art__caption">
							<p>Date bancare / Payment conditions S.C. SMART FITNESS STUDIO S.R.L.<br>
								VAT number: RO 30973537<br>
								BRD - RO72BRDE360SV63622543600
							</p>
						</div>
						<a href="" class="btn btn-secondary"><?=BTN_MORE;?></a>
					</article>
				</div>
			</div>
		</section> <!-- /.bar -->
		<hr>
		<section class="bar bar-news mb-5">
			<div class="row">
				<div class="col-xl-6">
					<?php include 'z-article.php';?>
				</div>
				<div class="col-xl-6">
					<?php include 'z-article.php';?>
				</div>
				<div class="col-xl-6">
					<?php include 'z-article.php';?>
				</div>
				<div class="col-xl-6">
					<?php include 'z-article.php';?>
				</div>
			</div>
		</section> <!-- /.bar -->
	</div> <!-- /.container -->
</main> <!-- /main -->

<?php
require_once 'includes/_footer.php';
require_once 'includes/_html_footer.php';
require_once '_deinit.php';
?>