<?php
session_start();
ob_start('ob_gzhandler');
// error_reporting(0);
// ini_set('display_errors', 'On');
define('BASE_DIR', dirname(__FILE__).DIRECTORY_SEPARATOR);
$s_page = basename($_SERVER['PHP_SELF']);
require 'parameters/parameters_site.php';
define('HOST', 'https://smartfit.dev/');
define('HOST_CDNA', HOST.'assets/');
define('HOST_CDND', HOST.'data_files/');
date_default_timezone_set(APP_DEFAULT_TIMEZONE);
setlocale(LC_TIME, array('ro.utf-8', 'ro_RO.UTF-8', 'ro_RO.utf-8', 'ro', 'ro_RO', 'ro_RO.ISO8859-2'));
//language
require BASE_DIR.'language/en.php';
?>