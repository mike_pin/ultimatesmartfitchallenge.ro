<?php
//*************************************************************************************************
// LIBRARY 	:	PARAMETERS_SITE.PHP
// ACTION	:	different parameters
//*************************************************************************************************
define('APP_DEFAULT_TIMEZONE', 'Europe/Bucharest');
define('DATE_FORMAT', 'd/m/Y');
define('CLIENT_NAME','SmartFit');
define('SITE_NAME','Ultimate Smartfit Challenge');
define('SITE_WEB','ultimatesmartfitchallenge.ro/');
define('META_TITLE', 'Ultimate Smartfit Challenge - REVEAL THE HERO IN YOU');
define('SOCIAL_LINK_FB','https://www.facebook.com/UltimateSmartfitChallenge/');
define('SOCIAL_LINK_TT','https://twitter.com/USC_by_Smartfit');
define('SOCIAL_LINK_YT','https://www.youtube.com/user/SmartfitStudio');
define('SOCIAL_LINK_IG','https://www.instagram.com/smartfit_studio/');
?>