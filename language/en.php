<?php
// NAVIGATION
// ==============================
define('NAV_HOME', 'HOME');
define('NAV_ABOUT', 'ABOUT U.S.C.');
define('NAV_RULEBOOK', 'RULEBOOK');
define('NAV_PARTNERS', 'PARTNERS');
define('NAV_CONTACT', 'CONTACT');
define('NAV_REGISTER', 'REGISTER');
define('NAV_LOGIN', 'LOGIN');


// HOMEPAGE
// ==============================
define('REGISTRATION_CALL', 'Join the newest and most spectacular sporting competition in Romania.');

// ABOUT US
// ==============================
define('ABOUT_PTITLE', 'ABOUT US');

// RULEBOOK
// ==============================
define('RULEBOOK_PTITLE', 'RULEBOOK');

// PARTNERS
// ==============================
define('PARTNERS_PTITLE', 'PARTNERS');
define('PARTNERS_FORM_TITLE', 'I want to support the competition!');
define('PARTNER_REQUEST', 'Doresc sa primesc oferta de promovare a sponsorului');
define('PARTNER_SUBMIT', 'SUBMIT');

// CONTACT
// ==============================
define('CONTACT_PTITLE', 'CONTACT');

// REGISTER
// ==============================
define('REGISTER_PTITLE', 'REGISTER');

// LOGIN
// ==============================
define('LOGIN_PTITLE', 'LOGIN');
define('KEEP_SIGNED_IN', 'Keep me signed in');
define('FORGOT_PASS', 'Forgot password');

// PASSWORD RESET
// ==============================
define('RESET_PASS_PTITLE', 'Password Reset');
define('RESET_PASSWORD', 'Reset password');

// EVENTS
// ==============================
define('EVENTS_PTITLE', 'EVENTS');

// TESTIMONIALS
// ==============================
define('TESTIMONIALS_PTITLE', 'TESTIMONIALS');

// TERMS AND CONDITIONS
// ==============================
define('TERMS_PTITLE', 'TERMS & CONDITIONS');

// PAYMENT
// ==============================
define('PAYMENT_PTITLE', 'PAYMENT ISSUES');


// FOOTER
// ==============================
// Newsletter
define('NEWSLETTER_TITLE', 'NEWSLETTER');
define('NEWSLETTER_BTN', 'SIGN UP');
// Footer events
define('FOOTER_EVENTS', 'NEXT EVENT');
// Footer partner
define('PARTNERS_JOIN', 'BECOME A FRIEND & PARTNER!');
define('PARTNERS_JOIN_US_BTN', '<i class="far fa-handshake"></i> Join the Club!');

// Client
define('POWERED_BY', 'a competition powered by');
define('TERMS', 'Terms & Conditions');
define('PAYMENT', 'Payment issues');
define('RIGHTS', 'All rights reserved');
define('DEVELOPER', '<a href="http://www.mioritix-media.com" title="web design" rel="blank" class="refsite">Web design</a> and <a href="http://www.mioritix-media.com/web-design-programming.php" title="web development" rel="blank" class="refsite">web development</a> by <a href="http://www.mioritix-media.com/contact.php" title="Mioritix Media" rel="blank">Mioritix Media</a> // <a href="http://www.mioritix-media.com/web-agency-romania.php" title="complete web solutions" rel="blank" class="refsite">complete web solutions</a>');

// GENERAL
// ==============================
define('POSTED','Posted on');

// Forms
define('FNAME_LABEL', 'First name');
define('FNAME_PLACEHOLDER', 'First name');
define('LNAME_LABEL', 'Last name');
define('LNAME_PLACEHOLDER', 'Last name');
define('EMAIL_LABEL', 'Email address');
define('EMAIL_PLACEHOLDER', 'Your email address');
define('COMPANY_LABEL', 'Company name');
define('COMPANY_PLACEHOLDER', 'The company\'s name');
define('PERSON_LABEL', 'Contact person');
define('PERSON_PLACEHOLDER', 'Who can we talk to?');
define('PHONE_LABEL', 'Phone number');
define('PHONE_PLACEHOLDER', 'Your phone number');
define('USERNAME_LABEL', 'Username');
define('USERNAME_PLACEHOLDER', 'Username');
define('PASSWORD_LABEL', 'Password');
define('PASSWORD_PLACEHOLDER', 'Your password');
define('CONFIRM_PASS_LABEL', 'Confirm password');
define('CONFIRM_PASS_PLACEHOLDER', 'Retype password');
define('COUNTRY_LABEL', 'Country');
define('TSHIRT_LABEL', 'T-shirt size');
define('CATEGORY_LABEL', 'Category');
define('REGISTRATION_FEE_LABEL', 'Registration fee for qualification: 20 euro');
define('REGISTRATION_FEE_PAYMENT', 'Transfer bancar');
define('REGISTER', 'REGISTER');
define('LOGIN', 'LOGIN');

// Buttons & Links
define('BTN_REGISTER', 'REGISTER NOW!');
define('BTN_MORE', 'READ MORE');
define('VIEW_ALL', 'View all');
?>