<?php
require_once '_init.php';
require_once 'includes/_html_header.php';
require_once 'includes/_header.php';
?>

<main role="main">

	

	<div class="bar">
		<div class="container">
			<h1><?=PARTNERS_PTITLE;?></h1>
			<div class="row">
				<div class="col-md-8">
					<p>Mulțumim tuturor sponsorilor și partenerilor noștri care ne sprijină în acest proiect de dezvoltare și diversificare a sportului românesc.</p>
					<p>Credem în competițiile sportive și cultivarea spiritului competițional. Credem în curajul de a aborda obstacole mari și în depășirea lor, în atitudini pozitive și abordări inovative.</p>
					<p>Credem în performanţă si actiunea responsabilă.</p>
					<ul class="list-logos list-unstyled d-flex flex-wrap align-items-center justify-content-around">
						<li><a href="" class="partner"><img src="<?=HOST_CDNA;?>imgs/z-art-register.png" class="img-fluid"></a></li>
						<li><a href="" class="partner"><img src="<?=HOST_CDNA;?>imgs/logo.png" class="img-fluid"></a></li>
						<li><a href="" class="partner"><img src="https://dummyimage.com/300x600/cccccc/ffffff.png&text=300x600" class="img-fluid"></a></li>
						<li><a href="" class="partner"><img src="<?=HOST_CDNA;?>imgs/z-art-register.png" class="img-fluid"></a></li>
						<li><a href="" class="partner"><img src="<?=HOST_CDNA;?>imgs/z-art-register.png" class="img-fluid"></a></li>
						<li><a href="" class="partner"><img src="<?=HOST_CDNA;?>imgs/z-art-register.png" class="img-fluid"></a></li>
						<li><a href="" class="partner"><img src="<?=HOST_CDNA;?>imgs/z-art-register.png" class="img-fluid"></a></li>
						<li><a href="" class="partner"><img src="<?=HOST_CDNA;?>imgs/z-art-register.png" class="img-fluid"></a></li>
						<li><a href="" class="partner"><img src="<?=HOST_CDNA;?>imgs/z-art-register.png" class="img-fluid"></a></li>
						<li><a href="" class="partner"><img src="<?=HOST_CDNA;?>imgs/z-art-register.png" class="img-fluid"></a></li>
					</ul>
					<form class="needs-validation" novalidate>
						<h4><?=PARTNERS_FORM_TITLE;?></h4>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label for=""><?=EMAIL_LABEL;?><span class="text-danger">*</span></label>
									<input type="email" class="form-control" id="" placeholder="<?=EMAIL_PLACEHOLDER;?>" required>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label for=""><?=COMPANY_LABEL;?></label>
									<input type="text" class="form-control" id="" placeholder="<?=COMPANY_PLACEHOLDER;?>">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label for=""><?=PERSON_LABEL;?></label>
									<input type="text" class="form-control" id="" placeholder="<?=PERSON_PLACEHOLDER;?>">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label for=""><?=PHONE_LABEL;?><span class="text-danger">*</span></label>
									<input type="phone" class="form-control" id="" placeholder="<?=PHONE_PLACEHOLDER;?>" required>
								</div>	
							</div>
						</div>
						<div class="form-group custom-control custom-checkbox">
							<input class="custom-control-input" type="checkbox" value="" id="registration-sponsor" required>
							<label class="custom-control-label" for="registration-sponsor"><?=PARTNER_REQUEST;?><span class="text-danger">*</span></label>
						</div>
						<div class="mt-4 text-center"><button type="submit" class="btn btn-primary"><?=PARTNER_SUBMIT;?></button></div>
					</form>
				</div>
				<div class="col-md-4">
					<?php include_once 'z-sidebar.php';?>
				</div>
			</div> <!-- /.row -->
		</div> <!-- /.container -->
	</div> <!-- /.bar -->



</main> <!-- /main -->

<?php
require_once 'includes/_footer.php';
require_once 'includes/_html_footer.php';
require_once '_deinit.php';
?>