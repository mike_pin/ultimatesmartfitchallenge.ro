<?php
require_once '_init.php';
require_once 'includes/_html_header.php';
require_once 'includes/_header.php';
?>

<main role="main">



	<div class="bar">
		<div class="container">
			<h1><?=TERMS_PTITLE;?></h1>
			<p>THE ULTIMATESMARTFITCHALLENGE.RO WEBSITE, INCLUDING ITS SUBSITES (TYPICALLY ANY WEB PAGES WHICH END IN ULTIMATESMARTFITCHALLENGE.RO) THAT DISPLAY THESE TERMS, AND ALL SOFTWARE, PRODUCTS, APPLICATIONS, MOBILE SITES, FEATURES AND SERVICES MADE AVAILABLE, DISPLAYED OR OFFERED BY OR THROUGH OUR WEB SITE OR SUBSITES (COLLECTIVELY, THIS “SITE”) IS OWNED AND OPERATED BY OR ON BEHALF OF SC SMART FITNESS STUDIO, WITH AN OFFICE LOCATED AT 22, PODGORIEI, TIMISOARA, ROMANIA. YOUR ACCESS AND USE OF THIS SITE IS SUBJECT TO THE FOLLOWING TERMS AND CONDITIONS OF USE (“TERMS OF USE”) AND ALL APPLICABLE LAWS. PLEASE READ THESE TERMS OF USE CAREFULLY.</p>
			<p>BY ACCESSING, BROWSING, OR USING THIS SITE, YOU ACKNOWLEDGE THAT YOU HAVE READ, UNDERSTOOD, ACCEPTED, AND AGREED TO THESE TERMS OF USE WITHOUT LIMITATION OR QUALIFICATION. IF YOU DO NOT AGREE TO ALL OF THESE TERMS OF USE, DO NOT ACCESS, BROWSE, OR USE THIS SITE OR THE CONTENT OR SERVICES HEREON.</p>
			<p>CERTAIN PORTIONS OF THE WEBSITE MAY BE SUBJECT TO ADDITIONAL TERMS AND CONDITIONS SPECIFIED BY US FROM TIME TO TIME; YOUR USE OF THE WEBSITE IS SUBJECT TO THOSE ADDITIONAL TERMS AND CONDITIONS, WHICH ARE INCORPORATED INTO THESE TERMS OF USE BY THIS REFERENCE.</p>
			<h2>I. Privacy Policy</h2>
			<p>We respect your privacy. Our collection, use and disclosure practices regarding yourpersonally identifiable information is set forth in our Privacy Policy. Please take some time to review our Privacy Policy as its terms are incorporated herein by reference and your assent to these Terms of Use includes your assent to our Privacy Policy.</p>
			<h2>II. Registration and Eligibility</h2>
			<p>Ultimate Smartfit Challenge may allow users to establish an account to access various features of this Site, such as registration for the Ultimate Smartfit Challenge Newsletter, events, registration for Ultimate Smartfit Challenge competitions and use of its message boards, discussion boards, and other public forums.</p>
		</div>
	</div> <!-- /.bar -->



</main> <!-- /main -->

<?php
require_once 'includes/_footer.php';
require_once 'includes/_html_footer.php';
require_once '_deinit.php';
?>