<?php
require_once '_init.php';
require_once 'includes/_html_header.php';
require_once 'includes/_header.php';
?>

<main role="main">

	

	<div class="bar">
		<div class="container">
			<h1><?=RESET_PASS_PTITLE;?></h1>
			<div class="row">
				<div class="col-md-8">
					<form class="needs-validation" novalidate>
						<div class="row">
							<div class="col-lg-8 offset-lg-2">
								<p>To reset your password, please enter your email address below</p>
								<div class="form-group">
									<label for=""><?=EMAIL_LABEL;?><span class="text-danger">*</span></label>
									<input type="text" class="form-control" placeholder="<?=EMAIL_PLACEHOLDER;?>" required>
								</div>
								<div class="mt-4 text-center"><button type="submit" class="btn btn-primary"><?=RESET_PASSWORD;?></button></div>
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-4">
					<?php include_once 'z-sidebar.php';?>
				</div>
			</div> <!-- /.row -->
		</div> <!-- /.container -->
	</div> <!-- /.bar -->



</main> <!-- /main -->

<?php
require_once 'includes/_footer.php';
require_once 'includes/_html_footer.php';
require_once '_deinit.php';
?>