<footer class="site-footer">
	<div class="bar bar--red">
		<div class="container">
			<div class="row">
				<div class="col-md-4 text-center text-md-left">
					<h4><?=NEWSLETTER_TITLE;?></h4>
					<form class="needs-validation" novalidate>
						<div class="form-group">
							<input type="email" class="form-control" id="" placeholder="<?=EMAIL_PLACEHOLDER;?>" required>
						</div>
						<button type="submit" class="btn btn-dark"><?=NEWSLETTER_BTN;?></button>
					</form>
				</div>
				<div class="col-md-4 text-center">
					<div class="footer-events">
						<h4 class="text-center">
							<?=FOOTER_EVENTS;?>: ROMANIAN THROWDOWN 2018
						</h4>
						<div class="countdown2event"></div>
					</div>
				</div>
				<div class="col-md-4 text-center text-md-right">
					<h4><?=PARTNERS_JOIN;?></h4>
					<a href="<?=HOST;?>partners.php" class="btn btn-lg btn-dark mt-3"><?=PARTNERS_JOIN_US_BTN;?></a>
				</div>
			</div>
		</div>
	</div> <!-- /.bar -->
	<div class="bar bar--secondary bar-client">
		<div class="container">
			<div class="row">
				<div class="col-md-4 text-center text-md-left">
					<?=POWERED_BY;?> <a href="<?=HOST;?>"><?=CLIENT_NAME;?></a>
				</div>
				<div class="col-md-4 client-info text-center">
					S.C. Smart Fitness Studio S.R.L.<br>
					V.A.T. nr.: RO30973537<br>
					BRD – RO72BRDE360SV63622543600
				</div>
				<div class="col-md-4 text-center text-md-right">
					<ul class="list-unstyled mb-0">
						<li class="li"><a href="<?=HOST;?>terms-conditions.php"><?=TERMS;?></a></li>
						<li class="li"><a href="<?=HOST;?>payment.php"><?=PAYMENT;?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div> <!-- /.bar -->
	<div class="bar bar--dark bar-dev">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<p class="copyright">Copyright &copy; <?=((date('Y') > 2018)?'2018 - ':'');?><?=date('Y');?>. <?=RIGHTS;?> <a href="<?=HOST;?>"><?=CLIENT_NAME;?></a>.</p>
				</div>
				<div class="col-md-6">
					<p class="developers text-md-right"><?=DEVELOPER;?></p>
				</div>
			</div>
		</div>
	</div> <!-- /.bar -->
</footer>