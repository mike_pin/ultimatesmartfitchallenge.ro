	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script>!window.jQuery && document.write('<script src="<?=HOST_CDNA;?>js/jquery-3.2.1.slim.min.js"><\/script>')</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script>if(typeof($.popper) === 'undefined') {document.write('<script src="<?=HOST_CDNA;?>js/popper.min.js"><\/script>')}</script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script>if(typeof($.fn.modal) === 'undefined') {document.write('<script src="<?=HOST_CDNA;?>js/bootstrap.min.js"><\/script>')}</script>
	
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
	<script>if(typeof(FontAwesome) === 'undefined') {document.write('<script src="<?=HOST_CDNA;?>js/fontawesome-all.js"><\/script>')}</script>
	<script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"></script>
	<script>if(typeof($.matchHeight) === 'undefined') {document.write('<script src="<?=HOST_CDNA;?>js/jquery.matchHeight-min.js"><\/script>')}</script>
	<script>
		// Set the date we're counting down to
		var countDownDate = new Date("Apr 1, 2018 12:00:00").getTime();
	</script>
	<script src="<?=HOST_CDNA;?>js/main.js"></script>

	<!-- BOOTSTRAP CDN FALLBACK CSS-->
	<div id="bootstrapCssTest" class="d-none"></div>
	<script>
	if ($('#bootstrapCssTest').is(':visible') === true) {
		$("head").prepend("<link rel='stylesheet' href='<?=HOST_CDNA;?>css/bootstrap.min.css' type='text/css' media='screen'>");
	}
	</script>
</body>
</html>