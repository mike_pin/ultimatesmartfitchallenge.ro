<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="<?=HOST_CDNA;?>css/main.css">

	<title><?=META_TITLE;?></title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<!-- Facebook -->
	<!-- <meta property="fb:admins" content="100000453215119" /> -->
	<!-- <meta property="fb:app_id" content="238699576661593" /> -->
	<meta property="og:title" content="" />
	<meta property="og:type" content="article" />
	<meta property="og:image" content="" />
	<meta property="og:url" content="" />
	<meta property="og:site_name" content="<?=SITE_NAME;?>" />
	<meta property="og:description" content="" />
	<!-- Google+ -->
	<meta itemprop="name" content="" />
	<meta itemprop="description" content="" />
	<meta itemprop="image" content="" />
	<!-- Twitter -->
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="<?=SITE_NAME;?>" />
	<meta name="twitter:title" content="" />
	<meta name="twitter:description" content="" />
	<meta name="twitter:image" content="" />
	<!-- Favicon -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?=HOST_CDNA;?>apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?=HOST_CDNA;?>favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?=HOST_CDNA;?>favicon-16x16.png">
	<link rel="manifest" href="<?=HOST_CDNA;?>site.webmanifest">
	<link rel="mask-icon" href="<?=HOST_CDNA;?>safari-pinned-tab.svg" color="#231f20">
	<link rel="shortcut icon" href="<?=HOST_CDNA;?>favicon.ico">
	<meta name="msapplication-TileColor" content="#231f20">
	<meta name="msapplication-config" content="<?=HOST_CDNA;?>browserconfig.xml">
	<meta name="theme-color" content="#231f20">
</head>
<body>