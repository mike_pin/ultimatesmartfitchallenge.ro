<div class="site-brand d-none d-md-block">
	<div class="container">
		<div class="row">
			<div class="col-6">
				<a href="<?=HOST;?>" class="logo"><img src="<?=HOST_CDNA;?>imgs/logo.png" class="img-fluid" alt="<?=SITE_NAME;?>"></a>
			</div>
			<div class="col-6 d-flex justify-content-end align-items-center">
				<a href="<?=SOCIAL_LINK_FB;?>" class="sf-social" title="<?=SITE_NAME;?> facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
				<a href="<?=SOCIAL_LINK_TT;?>" class="sf-social" title="<?=SITE_NAME;?> twitter" target="_blank"><i class="fab fa-twitter"></i></a>
				<a href="<?=SOCIAL_LINK_YT;?>" class="sf-social" title="<?=SITE_NAME;?> youtube" target="_blank"><i class="fab fa-youtube"></i></a>
				<a href="<?=SOCIAL_LINK_IG;?>" class="sf-social" title="<?=SITE_NAME;?> instagram" target="_blank"><i class="fab fa-instagram"></i></a>
			</div>
		</div>
	</div>
</div> <!-- /.site-brand -->

<nav class="site-nav navbar navbar-expand-md sticky-top">
	<a class="navbar-brand d-md-none" href="<?=HOST;?>"><img src="<?=HOST_CDNA;?>imgs/logo.png" alt="<?=SITE_NAME;?>"></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#site-nav">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse justify-content-center" id="site-nav">
		<ul class="navbar-nav nav nav-pills">
			<li class="nav-item active"><a class="nav-link" href="<?=HOST;?>"><?=NAV_HOME;?></a></li>
			<li class="nav-item"><a class="nav-link" href="<?=HOST;?>about.php"><?=NAV_ABOUT;?></a></li>
			<li class="nav-item"><a class="nav-link" href="<?=HOST;?>rulebook.php"><?=NAV_RULEBOOK;?></a></li>
			<li class="nav-item"><a class="nav-link" href="<?=HOST;?>partners.php"><?=NAV_PARTNERS;?></a></li>
			<li class="nav-item"><a class="nav-link" href="<?=HOST;?>contact.php"><?=NAV_CONTACT;?></a></li>
			<li class="nav-item"><a class="nav-link" href="<?=HOST;?>register.php"><?=NAV_REGISTER;?></a></li>
			<li class="nav-item"><a class="nav-link" href="<?=HOST;?>login.php"><?=NAV_LOGIN;?></a></li>
			<li class="d-md-none text-center mt-2 mb-3">
				<a href="<?=SOCIAL_LINK_FB;?>" class="sf-social" title="<?=SITE_NAME;?> facebook" target="_blank"><i class="fab fa-2x fa-facebook-f"></i></a>
				<a href="<?=SOCIAL_LINK_TT;?>" class="sf-social" title="<?=SITE_NAME;?> twitter" target="_blank"><i class="fab fa-2x fa-twitter"></i></a>
				<a href="<?=SOCIAL_LINK_YT;?>" class="sf-social" title="<?=SITE_NAME;?> youtube" target="_blank"><i class="fab fa-2x fa-youtube"></i></a>
				<a href="<?=SOCIAL_LINK_IG;?>" class="sf-social" title="<?=SITE_NAME;?> instagram" target="_blank"><i class="fab fa-2x fa-instagram"></i></a>
			</li>
		</ul>
	</div>
</nav> <!-- /.site-nav -->