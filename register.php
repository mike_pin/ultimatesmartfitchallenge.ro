<?php
require_once '_init.php';
require_once 'includes/_html_header.php';
require_once 'includes/_header.php';
?>

<main role="main">

	

	<div class="bar">
		<div class="container">
			<h1><?=REGISTER_PTITLE;?></h1>
			<div class="row">
				<div class="col-md-8">
					<h3>Date bancare / Payment conditions</h3>
					<p>S.C. SMART FITNESS STUDIO S.R.L.  / VAT number: RO 30973537 / BRD – RO72BRDE360SV63622543600</p>
					<form class="needs-validation" novalidate>
						<div class="row">
							<div class="col-lg-4 form-group">
								<label for=""><?=USERNAME_LABEL;?><span class="text-danger">*</span></label>
								<input type="text" class="form-control" placeholder="<?=USERNAME_PLACEHOLDER;?>" required>
							</div>
							<div class="col-lg-4 form-group">
								<label for=""><?=PASSWORD_LABEL;?><span class="text-danger">*</span></label>
								<input type="password" class="form-control" placeholder="<?=PASSWORD_PLACEHOLDER;?>" required>
							</div>
							<div class="col-lg-4 form-group">
								<label for=""><?=CONFIRM_PASS_LABEL;?><span class="text-danger">*</span></label>
								<input type="password" class="form-control" placeholder="<?=CONFIRM_PASS_PLACEHOLDER;?>" required>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 form-group">
								<label for=""><?=FNAME_LABEL;?><span class="text-danger">*</span></label>
								<input type="text" class="form-control" placeholder="<?=FNAME_PLACEHOLDER;?>" required>
							</div>
							<div class="col-lg-6 form-group">
								<label for=""><?=LNAME_LABEL;?><span class="text-danger">*</span></label>
								<input type="text" class="form-control" placeholder="<?=LNAME_PLACEHOLDER;?>" required>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 form-group">
								<label for=""><?=EMAIL_LABEL;?><span class="text-danger">*</span></label>
								<input type="text" class="form-control" placeholder="<?=EMAIL_PLACEHOLDER;?>" required>
							</div>
							<div class="col-lg-6 form-group">
								<label for=""><?=PHONE_LABEL;?></label>
								<input type="text" class="form-control" placeholder="<?=PHONE_PLACEHOLDER;?>">
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 form-group">
								<label for=""><?=COUNTRY_LABEL;?><span class="text-danger">*</span></label>
								<select class="custom-select" required>
									<option value="">do something</option>
									<option value="1">select this</option>
									<option value="2">select that</option>
									<option value="3">select whatever</option>
									<option value="4">just select</option>
								</select>
							</div>
							<div class="col-lg-6 form-group">
								<label for=""><?=TSHIRT_LABEL;?><span class="text-danger">*</span></label>
								<select class="custom-select" required>
									<option value="">do something</option>
									<option value="1">select this</option>
									<option value="2">select that</option>
									<option value="3">select whatever</option>
									<option value="4">just select</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col form-group">
								<label><?=CATEGORY_LABEL;?><span class="text-danger">*</span></label><br>
								<div class="custom-control custom-radio custom-control-inline">
									<input class="custom-control-input" type="radio" name="registration-cat" id="registration-cat-1" value="RX male" required>
									<label class="custom-control-label" for="registration-cat-1">RX male</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input class="custom-control-input" type="radio" name="registration-cat" id="registration-cat-2" value="RX female" required>
									<label class="custom-control-label" for="registration-cat-2">RX female</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input class="custom-control-input" type="radio" name="registration-cat" id="registration-cat-3" value="SCALED male" required>
									<label class="custom-control-label" for="registration-cat-3">SCALED male</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input class="custom-control-input" type="radio" name="registration-cat" id="registration-cat-4" value="SCALED female" required>
									<label class="custom-control-label" for="registration-cat-4">SCALED female</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col form-group">
								<label><?=REGISTRATION_FEE_LABEL;?><span class="text-danger">*</span></label>
								<div class="custom-control custom-checkbox">
									<input class="custom-control-input" type="checkbox" value="" id="registration-pay" required>
									<label class="custom-control-label" for="registration-pay"><?=REGISTRATION_FEE_PAYMENT;?></label>
								</div>
							</div>
						</div>
						<div class="mt-4 text-center">
							<button type="submit" class="btn btn-primary"><?=REGISTER;?></button>
							<a href="<?=HOST;?>login.php" class="btn btn-secondary"><?=LOGIN;?></a>
						</div>
					</form>
				</div>
				<div class="col-md-4">
					<?php include_once 'z-sidebar.php';?>
				</div>
			</div> <!-- /.row -->
		</div> <!-- /.container -->
	</div> <!-- /.bar -->



</main> <!-- /main -->

<?php
require_once 'includes/_footer.php';
require_once 'includes/_html_footer.php';
require_once '_deinit.php';
?>