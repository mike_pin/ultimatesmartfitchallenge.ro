<?php
require_once '_init.php';
require_once 'includes/_html_header.php';
require_once 'includes/_header.php';
?>

<main role="main">


	
	<div class="bar">
		<div class="container">
			<h1><?=ABOUT_PTITLE;?></h1>
			<p>[U.S.C] Ultimate Smartfit Challenge is a sports competition made by Smartfit.</p>
			<p>Born in 2014, first as a local contest for Ultimate Training, Functional Training, Crossfit devotees, it was a real success. By the years we developed a concept which is in present grown by more than one section.</p>
		</div>
	</div> <!-- /.bar -->
	<div class="bar bar--grey">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<h2>The New Generation of Heroes</h2>
					<p>The New Generation of Heroes is a contest addressed to the beginners, which offers them the possibility to taste the real feeling of a sports competition, with the purpose of revealing new heroes.</p>
				</div>
				<div class="col-md-4">
					<img src="<?=HOST_CDNA;?>imgs/z-art-register.png" class="img-fluid d-block mx-auto">
				</div>
			</div>
		</div>
	</div> <!-- /.bar -->
	<div class="bar">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<h2>Ultimate Smartfit Challenge – Team edition</h2>
					<p>Ultimate Smartfit Challenge – Team edition brings to the highlight all the Smartfit’s fundamentals, summing up the member’s strength, ambition, and perseverance, all link by a really strong coordination.</p>
				</div>
				<div class="col-md-4">
					<img src="<?=HOST_CDNA;?>imgs/z-art-register.png" class="img-fluid d-block mx-auto">
				</div>
			</div>
		</div>
	</div> <!-- /.bar -->
	<div class="bar bar--grey">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<h2>Romanian Throwdown by USC</h2>
					<p>Romanian Throwdown by USC takes this competition to the next level, an international one, which, in this year, we aspire to be.</p>
				</div>
				<div class="col-md-4">
					<img src="<?=HOST_CDNA;?>imgs/z-art-register.png" class="img-fluid d-block mx-auto">
				</div>
			</div>
		</div>
	</div> <!-- /.bar -->


	
</main> <!-- /main -->

<?php
require_once 'includes/_footer.php';
require_once 'includes/_html_footer.php';
require_once '_deinit.php';
?>