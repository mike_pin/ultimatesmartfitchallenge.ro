<?php
require_once '_init.php';
require_once 'includes/_html_header.php';
require_once 'includes/_header.php';
?>

<main role="main">

	

	<div class="bar">
		<div class="container">
			<h1><?=PAYMENT_PTITLE;?></h1>
			<div class="row">
				<div class="col-md-8">
					<h2>Payment conditions</h2>
					<p>The payment can be done on-line, at the moment when the order is placed. In case of order cancellation caused by the customer/at the initiative of the customer, the cost of returning the amounts paid in advance will be covered by the customer. All displayed prices on Ultimatesmartfitchallenge.ro are expressed in EUR and include VAT.</p>
					<p>
						SC Smart Fitness Studio SRL<br>
						Podgoriei, 22, Timișoara, Romania<br>
						V.A.T. nr.: RO30973537<br>
						BRD Roumania – RO72BRDE360SV63622543600
					</p>
				</div>
				<div class="col-md-4">
					<?php include_once 'z-sidebar.php';?>
				</div>
			</div> <!-- /.row -->
		</div> <!-- /.container -->
	</div> <!-- /.bar -->



</main> <!-- /main -->

<?php
require_once 'includes/_footer.php';
require_once 'includes/_html_footer.php';
require_once '_deinit.php';
?>