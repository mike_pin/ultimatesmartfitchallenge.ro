<?php
require_once '_init.php';
require_once 'includes/_html_header.php';
require_once 'includes/_header.php';
?>

<main role="main">


	
	<div class="bar">
		<div class="container">
			<h1><?=CONTACT_PTITLE;?></h1>
			<p>
				If you have a question about the Ultimate Smartfit Challenge, please feel free to contact our team:<br>
				hero[at]ultimatesmartfitchallenge.ro
			</p>
		</div>
	</div> <!-- /.bar -->
	<div class="bar bar--grey">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h2>Smartfit Studio 1</h2>
					<p>
						<b>Bd. Cetăţii – colţ cu Calea Torontalului</b><br>
						300389 Timişoara, România<br>
						<b>tel</b>: 0724.728.471
					</p>
					<p>
						<b>ORAR</b><br>
						<b>Luni – Sâmbătă</b>: 07:00 – 23:00<br>
						<b>Duminică</b>: 11:00 – 16:00
					</p>
				</div>
				<div class="col-md-6">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5566.238831796135!2d21.218642!3d45.768797!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xeca6e97347ed25a8!2sSmartfit!5e0!3m2!1sen!2sro!4v1517841064181"></iframe>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- /.bar -->
	<div class="bar">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h2>Smartfit Studio 2</h2>
					<p>
						<b>Calea Buziașului nr.12 (giratoriul AEM – Electrotimiș)</b><br>
						300389 Timişoara, România<br>
						<b>tel</b>: 0741.332.000
					</p>
					<p>
						<b>ORAR</b><br>
						<b>Luni – Sâmbătă</b>: 06:30 – 24:00<br>
						<b>Duminică</b>: 10:00 – 16:00
					</p>
				</div>
				<div class="col-md-6">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11139.437483341242!2d21.255388!3d45.733915!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb209ebcb2e987628!2sSmartfit+Studio!5e0!3m2!1sen!2sro!4v1517841916334"></iframe>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- /.bar -->
	


	
</main> <!-- /main -->

<?php
require_once 'includes/_footer.php';
require_once 'includes/_html_footer.php';
require_once '_deinit.php';
?>