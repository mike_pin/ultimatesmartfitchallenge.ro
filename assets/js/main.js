// DUCUMENT READY
// ==============================
$( document ).ready(function() {
	// MATCH HEIGHT
	$('.bsimple__title').matchHeight();
});

// COUNTDOWN
// ==============================
var x = setInterval(function() {
	var now = new Date().getTime();
	var distance = countDownDate - now;

	var days = Math.floor(distance / (1000 * 60 * 60 * 24));
	var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
	var seconds = Math.floor((distance % (1000 * 60)) / 1000);

	var counter = document.getElementsByClassName("countdown2event");
	for(var i = 0; i < counter.length; i++){
		counter[i].innerHTML = "<span><b>" + days + "</b>days</span><span><b>" + hours + "</b>hours</span><span><b>" + minutes + "</b>minutes</span><span><b>" + seconds + "</b>seconds</span>";
	}

	if (distance < 0) {
		clearInterval(x);
		for(var i = 0; i < counter.length; i++){
			counter[i].innerHTML = "<div class='alert alert-info text-center'>THERE ARE NO EVENTS</h4>";
		}
	}
}, 1000);

// BOOTSTRAP FORM VALIDATION
// ==============================
(function() {
	'use strict';
	window.addEventListener('load', function() {
	  // Fetch all the forms we want to apply custom Bootstrap validation styles to
	  var forms = document.getElementsByClassName('needs-validation');
	  // Loop over them and prevent submission
	  var validation = Array.prototype.filter.call(forms, function(form) {
		form.addEventListener('submit', function(event) {
		  if (form.checkValidity() === false) {
			event.preventDefault();
			event.stopPropagation();
		  }
		  form.classList.add('was-validated');
		}, false);
	  });
	}, false);
})();