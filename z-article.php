<article class="sf-nwz">
	<header>
		<h3 class="nwz__title"><a href="<?=HOST;?>events-details.php">Romanian Throwdown 2017 interviews</a></h3>
		<?=POSTED;?> <time datetime="2018-02-02" class="nwz__date">02 feb 2018</time>
	</header>
	<div class="row">
		<div class="col-sm-3 col-md-4">
			<a href="<?=HOST;?>events-details.php" class="nwz__pic" title=""><img src="<?=HOST_CDNA;?>imgs/z-art-register.png" class="img-fluid" alt=""></a>
		</div>
		<div class="col-sm-9 col-md-8 clearfix">
			<p>Oksana Orobets from Ukraine- the winner of RX Female category @ Romanian Throwdown by USC. Congratulations, it was very nice to meet you! See you next year! (more…)</p>
			<a href="<?=HOST;?>events-details.php" class="btn btn-sm btn-secondary float-right"><?=BTN_MORE;?></a>
		</div>
	</div>
</article>