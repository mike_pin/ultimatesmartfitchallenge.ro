<?php
require_once '_init.php';
require_once 'includes/_html_header.php';
require_once 'includes/_header.php';
?>

<main role="main">

	

	<div class="bar">
		<div class="container">
			<h1><?=RULEBOOK_PTITLE;?></h1>
			<div class="row">
				<div class="col-md-8">
					<article>
						<h2>GENERAL RULES</h2>
						<ol>
							<li>To participate to any USC edition every participant must agree with this rulebook.</li>
							<li>If the participant gives false information when filling the registration form, he/ she will be disqualified.</li>
							<li>All the participants must register and pay the registration fee before the deadline imposed by the staff.</li>
							<li>The registration fee for the qualifiers is 20 €. The qualifiers will take place online, based on sent videos witch contain the execution of the indicated WODs in each week. At the end of the qualifiers period, after the judges analyze the participants performance, the results will be displayed on our website. The registration fee for the semifinals: 40 €.</li>
							<li>If the competition is cancelled, the registration fee will be refunded to all the participants.</li>
							<li>The participants must be at least 16 to register for this competition. All the participants under 18, must have a written consent from their parents at the registration for the semifinals.</li>
							<li>Registrations are done individually at the Smartfit receptions or online, by filling a registration form.</li>
							<li>It is the participants responsibility to auto-evaluate their abilities and register to the most appropriate category for their fitness level.</li>
							<li>There are 4 categories of participants: Women, Men SCALED and Women, Men RX.</li>
							<li>Each category has a limited number of places.</li>
							<li>The first 3 places of each category will be awarded.</li>
							<li>It is the athlete responsibility to be present at the time indicated by the USC staff in order to complete the registration form.</li>
							<li>USC has the right to change the categories number of places, if the staff considers there are too many or too few people registered.</li>
						</ol>
						<h2>EVENT RULES</h2>
						<ol>
							<li>All participants must show a sport behavior. All the contradictory discussions with other participants or with the staff members, sabotaging by any means the other participants WOD`s execution during the competition, can lead to disqualification.</li>
							<li>Before every event, it will be communicated and explained to the participants the standards for each exercise just once. All the information will be given by one of the Head Judges or Observers.</li>
							<li>It is the participants responsibility to be present at every event- briefing. Those who are not present at these technical sessions will lose the right to make an appeal to the specific event. If this happens twice the athlete will lose the right to make an appeal throughout the contest.</li>
							<li>Only the athlete or his coach has the right to make an appeal. The appeal can be made in the first 30 minutes after displaying the final ranking.</li>
							<li>Camera or telephone recorded videos, photos or any media means will not represent a good reason for changing the judge’s decision about the participants score. It is a decision that only the Head Judge or the Observer of the competition can take. They can take into account these proofs and they can make the final decision, without being influenced by external factors.</li>
							<li>If one of the athletes is not participating at one of the event he/ she will not be scored at that event, but he remains in the competition.</li>
							<li>Throughout the entire event, the participant will be judged by one person designated by the staff, which will be called JUDGE. This person doesn’t have the obligation to count out loud the reps or to offer suggestions about the execution of the movements. The communication between the athletes and the judges has to be formal.</li>
							<li>The participants are responsible to execute the movements imposed for the specific event at the standards described at the beginning of the event. If they are not following the standards the judges will not score the reps.</li>
							<li>The organizers or the judges can, at any time during the competition, stop the competition if they think there is a high risk of injury for the participants.</li>
							<li>In case of a conflict between the judge and one of the athlete, the judge must warn him. If this happens again, the Observer must be called up in order to analyze if the athlete will be disqualified or not.</li>
							<li>Throughout the events, the athletes must wear the competition’s T-shirt, with the Ultimate Smartfit Challenge logo. When the events are completed, the participants can change their outfit.</li>
							<li>USC has the right to change and adjust the working conditions where the events are taking place, if they consider that these are influencing in any ways the execution of the movements or the arbitration process. These measures are taken for the good unfolding of the events and of the competition.</li>
						</ol>
						<h2>RANKING RULES</h2>
						<ol>
							<li>Each event has a specific number of points. The score will be told each athlete before the completion and it’s going to be on the athlete scorecard.</li>
							<li>The score for each event is given according to the athlete’s performance during the specific event. The criteria according to which the score is given, is written on athlete’s file.</li>
							<li>The ranking will be established according to the accumulated score during the event. The ranking order of each event will be communicated only at the end of the competition. The USC staff members who do the math do not have the obligation to communicate any result during the competition. These will communicate only with the Head Judges. The supporters of the USC heroes are not allowed to communicate with the calculations team.</li>
							<li>The staff can take the decision to make intermediate rankings during one stage of the competition if this represent elimination criteria. The rules and the number of athletes who are qualified for the next event will be communicated before the beginning of the competition. The result obtained at the end of each USC stage’s event can constitute a criterion to move on in the competition.</li>
							<li>If there are equal scores obtained during all the events it means that the athletes are on the same place on the final ranking. If the winners of the first three places have the same score they will share the prize.</li>
							<li>The equality of scores of two or more athletes in each event will lead to the sharing of the same place in the ranking. The elimination of one of the ranking’s places of a specific event is determinated only by the equality of the scores. This equality can eliminate even more than one place in the event’s ranking.</li>
							<li>The athletes from Women and Men Advanced category will be designated heroes.</li>
						</ol>
						<h2>VIDEO PENALTIES</h2>
						<ol>
							<li>for every no rep ( you don’t comply with the standards ) or missed rep ( you did 99 instead of 100 and moved forward ) 3 seconds penalty</li>
							<li>for every rep you didn’t finished after the time cap, 1 seconds penalty</li>
							<li>if you don’t have a timer present in the video at all time , 10 seconds penalty</li>
							<li>if you don’t present the equipment ( weights, heights ) before or after the workout, 5 minutes penalty</li>
						</ol>
					</article>
				</div>
				<div class="col-md-4">
					<?php include_once 'z-sidebar.php';?>
				</div>
			</div> <!-- /.row -->
		</div> <!-- /.container -->
	</div> <!-- /.bar -->



</main> <!-- /main -->

<?php
require_once 'includes/_footer.php';
require_once 'includes/_html_footer.php';
require_once '_deinit.php';
?>